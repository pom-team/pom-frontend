import { Model } from 'vue-mc'
import * as _ from 'lodash'

/**
 * Layer model
 */

export default class Layer extends Model {
  defaults () {
    return {
      id: Number,
      name: String,
      url: String,
      description: String,
      boundaries: Array,
      scale: Number,
      start_year: Date,
      end_year: Date,
      author: Number,
      sources: Array,
      attribution: String,
      further_readings: Array,
      is_default: Boolean,
      order: Number,
      is_visible: Boolean
    }
  }

  // Attribute mutations.
  mutations () {
    return {
      id: (id) => Number(id) || null,
      boundaries: (boundaries) => boundaries !== null ? boundaries['coordinates'] : null,
      // is_default:    (is_default) => defau ? 'checked' : '',
      is_visible: (is_visible) => !!this.is_default
    }
  }

  // Route configuration
  routes () {
    return {
      fetch: process.env.VUE_APP_ROOT_API + '/layers/{id}'
    }
  }
}
