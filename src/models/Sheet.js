import { Model } from 'vue-mc'

/**
 * Map Sheet model / for downloads
 */

// eslint-disable-next-line
export default class Sheet extends Model {
  defaults () {
    return {
      id: null,
      name: '',
      dropbox_link: '',
      boundaries: Array,
      year: '',
      layer: null,
      file_name: ''
    }
  }

  // Attribute mutations.
  mutations () {
    return {
      id: (id) => Number(id) || null,
      name: String,
      dropbox_link: String,
      boundaries: (boundaries) => boundaries !== null ? boundaries['coordinates'][0] : null,
      year: (year) => year !== null ? (new Date(year)).getFullYear() : null,
      layer: (layer) => Number(layer) || null,
      file_name: String
    }
  }

  // Route configuration
  routes () {
    return {
      fetch: process.env.VUE_APP_ROOT_API + '/sheets/{id}'
    }
  }
}
