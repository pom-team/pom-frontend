// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
// import Vue from 'vue/dist/vue.js'
// import Vue from 'vue/dist/vue.esm.js'
// import Vue from 'vue/dist/vue.runtime.esm.js'
import Vue from 'vue'
import Meta from 'vue-meta'
import router from './router'
import store from './store'
import App from './App'
import i18n from './i18n'
import BootstrapVue from 'bootstrap-vue'

Vue.use(Meta)
Vue.use(BootstrapVue)

// import L from 'vue2-leaflet'
Vue.config.productionTip = false

// eslint-disable-next-line
delete L.Icon.Default.prototype._getIconUrl

// eslint-disable-next-line
L.Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png')
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  router: router,
  store,
  i18n,
  template: '<App/>'
})
