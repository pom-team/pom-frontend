import { Collection } from 'vue-mc'
import Locality from '../models/Locality'
// import _ from 'lodash'

/**
 * Collection of localities
 */

export default class LocalityCollection extends Collection {
  model () {
    return Locality
  }

  routes () {
    return {
      fetch: process.env.VUE_APP_ROOT_API + '/localities/'
    }
  }
/* http://127.0.0.1:8000/api/localities/?search=lub
  getModelsFromResponse (response) {
    let localities = response.getData().features
    let localitiesCollection = []
    for (var l in localities) {
      let localityData = localities[l]
      let name = localityData.properties.name
      let changeStatusSince1945 = localityData.properties.change_status_since_1945
      let centroid = localityData.geometry.coordinates
      let locality = new Locality({id: localityData.id,
                                   name: name,
                                   changeStatusSince1945: changeStatusSince1945,
                                   centroid: centroid
                                 })
      localitiesCollection.push(locality)
    }
    return localitiesCollection
  } */
}
