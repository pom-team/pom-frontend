import { Model } from 'vue-mc'

/**
 * Locality model
 */

// eslint-disable-next-line
export default class Locality extends Model {
  defaults () {
    return {
      id: null,
      pom_id: String,
      name: '',
      centroid: {'coordinates': [0, 0]},
      change_status_since_1945: ''
    }
  }

  // Attribute mutations.
  mutations () {
    return {
      id: (id) => Number(id) || null,
      name: String,
      centroid: (centroid) => [centroid['coordinates'][1], centroid['coordinates'][0]]
    }
  }

  // Route configuration
  routes () {
    return {
      fetch: process.env.VUE_APP_ROOT_API + '/localities/{id}'
    }
  }
}
