import { Model } from 'vue-mc'

/**
 * Locality model
 */

// eslint-disable-next-line
export default class FullLocality extends Model {
  defaults () {
    return {
      id: null,
      pom_id: String,
      name: String,
      centroid: {'coordinates': [0, 0]},
      image: String,
      change_status_since_1945: String,
      destroyed: null,
      built_over: null,
      reappropriated: null,
      threatened: null,
      refugee_camp: null,
      illegal_settlement: null,
      establishment_date: Date,
      depopulation_date: Date,
      end_date: Date,
      population_group_1945: String,
      population_group_2016: String,
      comment: String,
      sources: []
    }
  }

  // Attribute mutations.
  mutations () {
    return {
      id: (id) => Number(id) || null,
      name: String,
      centroid: (centroid) => [centroid['coordinates'][1], centroid['coordinates'][0]],
      image: String
    }
  }

  // Route configuration
  routes () {
    return {
      fetch: process.env.VUE_APP_ROOT_API + '/localities/{id}'
    }
  }
}
