import { Collection } from 'vue-mc'
import Sheet from '../models/Sheet'
import _ from 'lodash'

/**
 * Collection of sheets
 */

export default class SheetCollection extends Collection {
  model () {
    return Sheet
  }

  routes () {
    return {
      fetch: process.env.VUE_APP_ROOT_API + '/sheets/'
      // fetch: process.env.VUE_APP_ROOT_API + '/sheets?lat=' + lat + '&lng=' + lng
    }
  }
/*
  getModelsFromResponse (response) {
    let localities = response.getData().features
    let localitiesCollection = []
    for (var l in localities) {
      let localityData = localities[l]
      let name = localityData.properties.name
      let changeStatusSince1945 = localityData.properties.change_status_since_1945
      let centroid = localityData.geometry.coordinates
      let locality = new Locality({id: localityData.id,
                                   name: name,
                                   changeStatusSince1945: changeStatusSince1945,
                                   centroid: centroid
                                 })
      localitiesCollection.push(locality)
    }
    return localitiesCollection
  } */
}
