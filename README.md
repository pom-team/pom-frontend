# Palestine Open Maps 

This is the main repository for the front end of the [PalOpenMaps.org](http://palopenmaps.org/) platform.

## Setup 
See pom-deployment to view the docker-compose files for setting up the entire platform, including the front end, back-end, and tile server, and osm-seed infrastructure. 

## Issues and Roadmap
Please report issues to the issue tracker on this repository. You can also see the issues that we're working on on the [board](https://gitlab.com/pom-team/pom-frontend/boards). 

## Code of Conduct
Short version: Be respectful to all your peers. 

Long version: view CodeOfConduct.md