import Vue from 'vue'
import Vuex from 'vuex'
// import Locality from './models/Locality'
// import Layer from './models/Layer'
// import Sheet from './models/Sheet'
import FullLocality from './models/FullLocality'
import LocalityCollection from './collections/LocalityCollection'
import LayerCollection from './collections/LayerCollection'
import SheetCollection from './collections/SheetCollection'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    localities: [],
    filterState: ['remaining', 'depopulated', 'depopulated_appropriated', 'depopulated_built_over'],
    filteredLocalities: [],
    selectedSheets: null,
    selectedLocality: null,
    fullLocality: null,
    layers: null,
    sheets: null,
    selectedLayer: null,
    basemaps: null,
    overlays: null,
    showLocalities: true,
    showOverlay: false,
    showSheets: false,
    showLocalityInfoPanel: false,
    showMapLegendsPanel: false,
    locale: 'en',
    hideFooter: false,
    downloadLink: '',
    screenSplitted: false
  },

  mutations: {
    updateLocalities(state, localitiesList) {
      state.localities = localitiesList
    },
    updateSelectedLocality(state, locality) {
      state.selectedLocality = locality
    },
    updateFullLocality(state, fullLocality) {
      state.fullLocality = fullLocality
    },
    setLayers(state, allLayers) {
      state.layers = allLayers
    },
    setSheets(state, allSheets) {
      state.sheets = allSheets
    },
    setSelectedLayer(state, selectedLayer) {
      state.selectedLayer = selectedLayer
    },
    setBasemaps(state, basemaps) {
      state.basemaps = basemaps
    },
    setOverlays(state, overlays) {
      state.overlays = overlays
    },
    toggleShowLocalities(state) {
      state.showLocalities = !state.showLocalities
    },
    setShowOverlay(state, showOverlayState) {
      state.showOverlay = showOverlayState
    },
    setShowLocalities(state, showLocalitiesState) {
      state.showLocalities = showLocalitiesState
    },
    setFilterState(state, filterState) {
      state.filterState = filterState
    },
    setFilteredLocalities(state, filterState) {
      if (filterState === null) {
        filterState = state.filterState
      }
      state.filteredLocalities = state.localities.filter((locality) => filterState.includes(locality.change_status_since_1945))
    },
    toggleSplitScreen (state) {
      state.screenSplitted = !state.screenSplitted
    },
    // todo: get sheets related to selected lat & lng
    setSelectedSheets(state, latlng) {
      // console.log(latlng.lat)
      // console.log(latlng.lng)
      let sheets = state.sheets.models
      // remove unvalid sheets
      let validSheets = sheets.filter(sheet => sheet.boundaries != null)
      // filter sheets
      state.selectedSheets = validSheets.filter(
        sheet => (
          (Number(Math.min(sheet.boundaries[0][0], sheet.boundaries[1][0], sheet.boundaries[2][0], sheet.boundaries[3][0])) < Number(latlng.lng)) &&
          (Number(Math.max(sheet.boundaries[0][0], sheet.boundaries[1][0], sheet.boundaries[2][0], sheet.boundaries[3][0])) > Number(latlng.lng)) &&
          (Number(Math.min(sheet.boundaries[0][1], sheet.boundaries[1][1], sheet.boundaries[2][1], sheet.boundaries[3][1])) < Number(latlng.lat)) &&
          (Number(Math.max(sheet.boundaries[0][1], sheet.boundaries[1][1], sheet.boundaries[2][1], sheet.boundaries[3][1])) > Number(latlng.lat))
        ))
      // console.log("Number of sheets: " + state.selectedSheets.length)
    },
    setShowLocalityInfoPanel(state, show) {
      state.showLocalityInfoPanel = show
    },
    setShowMapLegendsPanel(state, show) {
      state.showMapLegendsPanel = show
    },
    toggleShowLegends(state) {
      state.showMapLegendsPanel = !state.showMapLegendsPanel
    },
    setLocale(state, locale) {
      state.locale = locale
    },
    setHideFooter(state, hide) {
      state.hideFooter = hide
    },
    setDownloadLink(state, link) {
      // console.log(link)
      state.downloadLink = link
    },
    toggleSplitScreen (state) {
      state.screenSplitted = !state.screenSplitted
    },
  },

  getters: {
    // useful utility function
    getLocalityById: (state) => (id) => {
      return state.localities.find({ id: id })
    },
    getFullLocality: (state) => state.fullLocality,
    getSelectedLayer: (state) => state.selectedLayer,
    basemapLayers: (state) => state.localities.filter((layer) => !layer.is_overlay),
    overlayLayers: (state) => state.localities.filter((layer) => layer.is_overlay)
  },

  actions: {
    selectLocality({ commit, state }, locality) {
      commit('updateSelectedLocality', locality)
    },
    getLocalities(state) {
      return new Promise((resolve) => {
        let allLocalities = new LocalityCollection()
        allLocalities.fetch().then(response => {
          state.commit('updateLocalities', allLocalities)
          state.commit('setFilteredLocalities', null)
          resolve(response);
        })
      })
    },
    // get map sheets for download
    getSheets(state) {
      let allSheets = new SheetCollection()
      allSheets.fetch().then(() => {
        // console.log("All Sheets: " + allSheets)
        state.commit('setSheets', allSheets)
      })
    },
    // getSelectedSheets ({ commit, state, })
    getFullLocalityData({ commit, state }, id) {
      let fullLocality = new FullLocality({ id: id })
      fullLocality.fetch().then(() => {
        commit('updateFullLocality', fullLocality)
      })// .catch(())
    },
    getLayers(state) {
      let allLayers = new LayerCollection()
      allLayers.fetch().then(() => {
        // console.log(allLayers.find({is_default: true}))
        state.commit('setLayers', allLayers)
        state.commit('setSelectedLayer', allLayers.find({ is_default: true }))
        state.commit('setOverlays', allLayers.filter((layer) => layer.is_overlay))
        state.commit('setBasemaps', allLayers.filter((layer) => !layer.is_overlay))
      })// .catch(())
    }
  }
})
