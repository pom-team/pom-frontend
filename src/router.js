import Vue from 'vue'
import VueRouter from 'vue-router'
import IntroPage from '@/views/IntroPage'
import AboutPage from '@/views/AboutPage'
import VisionPage from '@/views/VisionPage'
import SupportPage from '@/views/SupportPage'
import NotFoundComponent from '@/views/NotFoundComponent'
import MapViewer from '@/components/MapViewer'
// import LocalityInfoPanel from '@/components/LocalityInfoPanel'

Vue.use(VueRouter)

export default new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'IntroPage',
      component: IntroPage
    },
    {
      path: '/about',
      name: 'AboutPage',
      component: AboutPage
    },
    {
      path: '/vision',
      name: 'VisionPage',
      component: VisionPage
    },
    {
      path: '/support',
      name: 'SupportPage',
      component: SupportPage
    },
    {
      path: '/view/:id?/:location?',
      name: 'MapViewer',
      component: MapViewer
    },
    {
      path: '*',
      component: NotFoundComponent
    }
  ]
})
