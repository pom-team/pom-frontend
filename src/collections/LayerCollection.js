import { Collection } from 'vue-mc'
import Layer from '../models/Layer'
import _ from 'lodash'

/**
 * Collection of layers
 */

export default class LayerCollection extends Collection {
  model () {
    return Layer
  }

  routes () {
    return {
      fetch: process.env.VUE_APP_ROOT_API + '/layers/'
    }
  }
}
